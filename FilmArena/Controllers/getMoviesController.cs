﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FilmArena.Controllers.Model;
using FilmArena.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace FilmArena.Controllers
{
    [Produces("application/json")]
    [Route("api/getMovies")]
    public class getMoviesController : Controller
    {
        private readonly MoviesContext _mcontext;
        private readonly ProducersContext _pcontext;
        private readonly ActorsContext _acontext;
        private readonly movievsactorsContext _macontext;
        private readonly movievsproducersContext _mpcontext;

        public getMoviesController(MoviesContext mcontext,ProducersContext pcontext,ActorsContext acontext,movievsactorsContext macontext,movievsproducersContext mpcontext)
        {
            _mcontext = mcontext;
            _pcontext = pcontext;
            _acontext = acontext;
            _mpcontext = mpcontext;
            _macontext = macontext;
        }

        // GET: api/getMovies
        [HttpGet]
        public object Get()
        {
            List<movielist> mlist = new List<movielist>();
            foreach (var temp in _mcontext.movies)
            {
                movielist m1;
                try
                {
                    m1 = new movielist();
                    m1.mid = temp.mid;
                    m1.mname = temp.mname;
                    m1.mplot = temp.mplot;
                    m1.mposter = temp.mposter;
                    m1.myear = temp.myear;
                    m1.lactor = new List<actors>();
                    m1.lproducers = new List<producers>();

                    foreach (var s1 in _mpcontext.movievsproducers.Where(o1 => o1.mpmovie == temp.mid).ToList())
                    {
                        var t1 = _pcontext.producers.FirstOrDefault(a => a.pid == s1.mpproducer);
                        if (t1 != null)
                            m1.lproducers.Add(t1);
                    }

                    foreach (var s2 in _macontext.movievsactors.Where(o1 => o1.mamovie == temp.mid).ToList())
                    {
                        var t1 = (actors)_acontext.actors.FirstOrDefault(a => a.aid == s2.maactor);
                        if (t1 != null)
                            m1.lactor.Add((actors)t1);
                    }

                    mlist.Add(m1);

                }catch(Exception ex)
                {
                    return ex;
                }
                finally
                {
                    m1 = null;
                }                

            }
            //JObject jo = JsonConvert.DeserializeObject<Object>(entryPoint);
            return mlist;
        }

        // GET: api/getMovies/5
        [HttpGet("{id}", Name = "Get")]
        public string Get(int id)
        {
            return "value";
        }
        
        // POST: api/getMovies
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }
        
        // PUT: api/getMovies/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }
        
        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
