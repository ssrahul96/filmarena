﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmArena.Controllers.Model;
using FilmArena.Models;

namespace FilmArena.Controllers
{
    [Produces("application/json")]
    [Route("api/actors")]
    public class actorsController : Controller
    {
        private readonly ActorsContext _context;

        public actorsController(ActorsContext context)
        {
            _context = context;
        }

        // GET: api/actors
        [HttpGet]
        public IEnumerable<actors> Getactors()
        {
            return _context.actors;
        }

        // GET: api/actors/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getactors([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actors = await _context.actors.SingleOrDefaultAsync(m => m.aid == id);

            if (actors == null)
            {
                return NotFound();
            }

            return Ok(actors);
        }

        // PUT: api/actors/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putactors([FromRoute] int id, [FromBody] actors actors)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != actors.aid)
            {
                return BadRequest();
            }

            _context.Entry(actors).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!actorsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/actors
        [HttpPost]
        public async Task<IActionResult> Postactors([FromBody] actors actors)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.actors.Add(actors);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getactors", new { id = actors.aid }, actors);
        }

        // DELETE: api/actors/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deleteactors([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var actors = await _context.actors.SingleOrDefaultAsync(m => m.aid == id);
            if (actors == null)
            {
                return NotFound();
            }

            _context.actors.Remove(actors);
            await _context.SaveChangesAsync();

            return Ok(actors);
        }

        private bool actorsExists(int id)
        {
            return _context.actors.Any(e => e.aid == id);
        }
    }
}