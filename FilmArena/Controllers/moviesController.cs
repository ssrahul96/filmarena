﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmArena.Controllers.Model;
using FilmArena.Models;

namespace FilmArena.Controllers
{
    [Produces("application/json")]
    [Route("api/movies")]
    public class moviesController : Controller
    {
        private readonly MoviesContext _context;

        public moviesController(MoviesContext context)
        {
            _context = context;
        }

        // GET: api/movies
        [HttpGet]
        public IEnumerable<movies> Getmovies()
        {
            return _context.movies;
        }

        // GET: api/movies/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getmovies([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var movies = await _context.movies.SingleOrDefaultAsync(m => m.mid == id);

            if (movies == null)
            {
                return NotFound();
            }

            return Ok(movies);
        }

        // PUT: api/movies/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putmovies([FromRoute] int id, [FromBody] movies movies)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != movies.mid)
            {
                return BadRequest();
            }

            _context.Entry(movies).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!moviesExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/movies
        [HttpPost]
        public async Task<IActionResult> Postmovies([FromBody] movies movies)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.movies.Add(movies);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getmovies", new { id = movies.mid }, movies);
        }

        // DELETE: api/movies/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletemovies([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var movies = await _context.movies.SingleOrDefaultAsync(m => m.mid == id);
            if (movies == null)
            {
                return NotFound();
            }

            _context.movies.Remove(movies);
            await _context.SaveChangesAsync();

            return Ok(movies);
        }

        private bool moviesExists(int id)
        {
            return _context.movies.Any(e => e.mid == id);
        }
    }
}