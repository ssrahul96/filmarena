﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmArena.Models;

namespace FilmArena.Model
{
    [Produces("application/json")]
    [Route("api/movievsactors")]
    public class movievsactorsController : Controller
    {
        private readonly movievsactorsContext _context;

        public movievsactorsController(movievsactorsContext context)
        {
            _context = context;
        }

        // GET: api/movievsactors
        [HttpGet]
        public IEnumerable<movievsactors> Getmovievsactors()
        {
            return _context.movievsactors;
        }

        // GET: api/movievsactors/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getmovievsactors([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var movievsactors = await _context.movievsactors.SingleOrDefaultAsync(m => m.maID == id);

            if (movievsactors == null)
            {
                return NotFound();
            }

            return Ok(movievsactors);
        }

        // PUT: api/movievsactors/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putmovievsactors([FromRoute] int id, [FromBody] movievsactors movievsactors)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != movievsactors.maID)
            {
                return BadRequest();
            }

            _context.Entry(movievsactors).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!movievsactorsExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/movievsactors
        [HttpPost]
        public async Task<IActionResult> Postmovievsactors([FromBody] movievsactors movievsactors)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.movievsactors.Add(movievsactors);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getmovievsactors", new { id = movievsactors.maID }, movievsactors);
        }

        // DELETE: api/movievsactors/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletemovievsactors([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var movievsactors = await _context.movievsactors.SingleOrDefaultAsync(m => m.maID == id);
            if (movievsactors == null)
            {
                return NotFound();
            }

            _context.movievsactors.Remove(movievsactors);
            await _context.SaveChangesAsync();

            return Ok(movievsactors);
        }

        private bool movievsactorsExists(int id)
        {
            return _context.movievsactors.Any(e => e.maID == id);
        }
    }
}