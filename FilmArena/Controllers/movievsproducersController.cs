﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmArena.Model;
using FilmArena.Models;

namespace FilmArena.Controllers
{
    [Produces("application/json")]
    [Route("api/movievsproducers")]
    public class movievsproducersController : Controller
    {
        private readonly movievsproducersContext _context;

        public movievsproducersController(movievsproducersContext context)
        {
            _context = context;
        }

        // GET: api/movievsproducers
        [HttpGet]
        public IEnumerable<movievsproducers> Getmovievsproducers()
        {
            return _context.movievsproducers;
        }

        // GET: api/movievsproducers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getmovievsproducers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var movievsproducers = await _context.movievsproducers.SingleOrDefaultAsync(m => m.mpID == id);

            if (movievsproducers == null)
            {
                return NotFound();
            }

            return Ok(movievsproducers);
        }

        // PUT: api/movievsproducers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putmovievsproducers([FromRoute] int id, [FromBody] movievsproducers movievsproducers)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != movievsproducers.mpID)
            {
                return BadRequest();
            }

            _context.Entry(movievsproducers).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!movievsproducersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/movievsproducers
        [HttpPost]
        public async Task<IActionResult> Postmovievsproducers([FromBody] movievsproducers movievsproducers)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.movievsproducers.Add(movievsproducers);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getmovievsproducers", new { id = movievsproducers.mpID }, movievsproducers);
        }

        // DELETE: api/movievsproducers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deletemovievsproducers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var movievsproducers = await _context.movievsproducers.SingleOrDefaultAsync(m => m.mpID == id);
            if (movievsproducers == null)
            {
                return NotFound();
            }

            _context.movievsproducers.Remove(movievsproducers);
            await _context.SaveChangesAsync();

            return Ok(movievsproducers);
        }

        private bool movievsproducersExists(int id)
        {
            return _context.movievsproducers.Any(e => e.mpID == id);
        }
    }
}