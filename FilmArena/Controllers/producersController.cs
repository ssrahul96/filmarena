﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using FilmArena.Controllers.Model;
using FilmArena.Models;

namespace FilmArena.Controllers
{
    [Produces("application/json")]
    [Route("api/producers")]
    public class producersController : Controller
    {
        private readonly ProducersContext _context;

        public producersController(ProducersContext context)
        {
            _context = context;
        }

        // GET: api/producers
        [HttpGet]
        public IEnumerable<producers> Getproducers()
        {
            return _context.producers;
        }

        // GET: api/producers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Getproducers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var producers = await _context.producers.SingleOrDefaultAsync(m => m.pid == id);

            if (producers == null)
            {
                return NotFound();
            }

            return Ok(producers);
        }

        // PUT: api/producers/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Putproducers([FromRoute] int id, [FromBody] producers producers)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != producers.pid)
            {
                return BadRequest();
            }

            _context.Entry(producers).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!producersExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/producers
        [HttpPost]
        public async Task<IActionResult> Postproducers([FromBody] producers producers)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.producers.Add(producers);
            await _context.SaveChangesAsync();

            return CreatedAtAction("Getproducers", new { id = producers.pid }, producers);
        }

        // DELETE: api/producers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Deleteproducers([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var producers = await _context.producers.SingleOrDefaultAsync(m => m.pid == id);
            if (producers == null)
            {
                return NotFound();
            }

            _context.producers.Remove(producers);
            await _context.SaveChangesAsync();

            return Ok(producers);
        }

        private bool producersExists(int id)
        {
            return _context.producers.Any(e => e.pid == id);
        }
    }
}