﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FilmArena.Controllers.Model;

namespace FilmArena.Models
{
    public class ActorsContext : DbContext
    {
        public ActorsContext (DbContextOptions<ActorsContext> options)
            : base(options)
        {
        }

        public DbSet<FilmArena.Controllers.Model.actors> actors { get; set; }
    }
}
