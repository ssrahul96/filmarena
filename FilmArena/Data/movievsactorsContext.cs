﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FilmArena.Model;

namespace FilmArena.Models
{
    public class movievsactorsContext : DbContext
    {
        public movievsactorsContext (DbContextOptions<movievsactorsContext> options)
            : base(options)
        {
        }

        public DbSet<FilmArena.Model.movievsactors> movievsactors { get; set; }
    }
}
