﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FilmArena.Model;

namespace FilmArena.Models
{
    public class movievsproducersContext : DbContext
    {
        public movievsproducersContext (DbContextOptions<movievsproducersContext> options)
            : base(options)
        {
        }

        public DbSet<FilmArena.Model.movievsproducers> movievsproducers { get; set; }
    }
}
