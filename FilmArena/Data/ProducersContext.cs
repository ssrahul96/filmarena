﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FilmArena.Controllers.Model;

namespace FilmArena.Models
{
    public class ProducersContext : DbContext
    {
        public ProducersContext (DbContextOptions<ProducersContext> options)
            : base(options)
        {
        }

        public DbSet<FilmArena.Controllers.Model.producers> producers { get; set; }
    }
}
