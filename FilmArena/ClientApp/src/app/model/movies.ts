﻿import { producers } from "./producers";
import { actors } from "./actors";

export class movies {
    mid: number;
    mname: string;
    myear: number;
    mplot: string
    mposter: string;
}

export class movielist {
    mid: number;
    mname: string;
    myear: number;
    mplot: string
    mposter: string;
    lproducer: producers[];
    lactor: actors[];
}
