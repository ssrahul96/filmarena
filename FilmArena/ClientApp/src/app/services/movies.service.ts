import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { movies, movielist } from '../model/movies';
import { Observable } from 'rxjs/Observable';
import { Body } from '@angular/http/src/body';
import { movievsproducer } from '../model/movievsproducer';
import { movievsactor } from '../model/movievsactor';
import { HttpClient } from '@angular/common/http'

@Injectable()
export class MoviesService {

  constructor(private _http: HttpClient) {

  }

  getMovies(): Observable<movies[]> {
    return this._http.get('/api/movies')
      .map(resp => resp as movies[]);
  }

  getMovieList(): Observable<movielist[]> {
    return this._http.get('/api/getmovies')
      .map(resp => resp as movielist[]);
  }

  addMovie(mov: movies): Observable<any> {
    return this._http.post('/api/movies', mov).map(resp => resp as any);
  }

  deleteMovie(id: number): void {
    this._http.delete('/api/movies/' + id).subscribe(x => console.log(x));
  }

  updateMovie(mov: movies): void {
    this._http.put('/api/movies/' + mov.mid, mov).subscribe(x => console.log(x));
  }

  addnewMovievsActor(data: movievsactor): Observable<any> {
    return this._http.post('/api/movievsactors/', data).map(resp => resp as any);
  }

  addnewMovievsProducer(data: movievsproducer): Observable<any> {
    return this._http.post('/api/movievsproducers/', data).map(resp => resp as any);
  }
}
