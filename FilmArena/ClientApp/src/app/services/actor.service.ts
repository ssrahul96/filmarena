import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { actors } from '../model/actors';
import { Observable } from 'rxjs/Observable';
import { Body } from '@angular/http/src/body';
import { HttpClient } from '@angular/common/http'

@Injectable()
export class ActorService {
  constructor(private _http: HttpClient) {

  }

  getActors(): Observable<actors[]> {
    return this._http.get('/api/actors')
      .map(resp => resp as actors[]);
  }

  addActor(act: actors): Observable<any> {
    return this._http.post('/api/actors', act).map(resp => resp as any);
  }

  deleteActor(id: number): void {
    this._http.delete('/api/actors/' + id).subscribe(x => console.log(x));
  }

  updateActor(act: actors): void {
    this._http.put('/api/actors/' + act.aid, act).subscribe(x => console.log(x));
  }
}
