import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { producers } from '../model/producers';
import { Observable } from 'rxjs/Observable';
import { Body } from '@angular/http/src/body';
import { HttpClient } from '@angular/common/http'

@Injectable()
export class ProducerService {
  constructor(private _http: HttpClient) {

  }

  getProducers(): Observable<producers[]> {
    return this._http.get('/api/producers')
      .map(resp => resp as producers[]);
  }

  addProducer(prod: producers): Observable<any> {
    return this._http.post('/api/producers', prod).map(resp => resp as any);
  }

  deleteProducer(id: number): void {
    this._http.delete('/api/producers/' + id).subscribe(x => console.log(x));
  }

  updateProducer(prod: producers): void {
    this._http.put('/api/producers/' + prod.pid, prod).subscribe(x => console.log(x));
  }
}
