import { Component, Pipe, PipeTransform } from '@angular/core';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
import { movies, movielist } from '../../model/movies';
import { MoviesService } from '../../services/movies.service';
import { ActorService } from '../../services/actor.service';
import { ProducerService } from '../../services/producer.service';
import { actors } from '../../model/actors';
import { producers } from '../../model/producers';
import { movievsactor } from '../../model/movievsactor';
import { movievsproducer } from '../../model/movievsproducer';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {
    actorlist: actors[];
    producerlist: producers[];
    movieCollection: movies[];
    editmoviestate: boolean;
    movieToEdit: movies;
    newMovie: any;
    movielists: movielist[];
    newactor: any;
    newproducer: any;
    actormap = new Map();
    producermap = new Map();
    newactorview = new Array<String>();
    newproducerview = new Array<String>();
    createnewhero_name: any;
    createnewproducer_name: any;



    ngOnInit(): void {
        //this.getMovies();
        //this.getMovieList();
        //this.getActors();
        //this.getProducers();
        this.editmoviestate = false;
    }


    constructor(private movieservice: MoviesService, private actorservice: ActorService, private producerservice: ProducerService) {
        this.newMovie = {
            mid: 0,
            mname: '',
            mplot: '',
            myear: 2018,
            mposter: '',
            lactor: new Set(),
            lproducer: new Set()
        };
    }

    getActors(): void {
        this.actorservice.getActors().subscribe(list => {
            this.actorlist = list;
            console.log(list);
            list.forEach(ele => {
                this.actormap.set(ele.aid, ele.aname);
            })
            this.newactor = this.actorlist[0].aid;
            console.log(this.newactor);
        },
            err => {
                //console.log(err);
            })
    }

    getProducers(): void {
        this.producerservice.getProducers().subscribe(list => {
            this.producerlist = list;

            list.forEach(ele => {
                this.producermap.set(ele.pid, ele.pname);
            })
            console.log(this.producermap);
            console.log(list);
            this.newproducer = this.producerlist[0].pid;
            console.log(this.newproducer);
        },
            err => {
                //console.log(err);
            })
    }

    getMovieList(): void {
        this.movieservice.getMovieList().subscribe(list => {
            this.movielists = list;
            console.log(list);
        },
            err => {
                //console.log(err);
            })
    }

    getMovies(): void {
        this.movieservice.getMovies().subscribe(moviedatas => {
            this.movieCollection = moviedatas;
        },
            err => {
                //console.log(err);
            })
    }


    addhero(event: actors) {
        console.log(event);
        this.newactor = event;
    }

    addproducer(event: producers) {
        console.log(event);
        this.newproducer = event;
    }

    addnewhero() {
        this.newMovie.lactor.add(this.newactor);
        console.log('newactor' + this.newactor);
        this.newactorview.push(this.actormap.get(parseInt(this.newactor)));
        this.newactorview = this.removeDuplicateUsingSet(this.newactorview);
        console.log('newactorview' + this.newactorview);
    }
    addnewproducer() {
        this.newMovie.lproducer.add(this.newproducer);
        console.log('newproducer' + this.newproducer);
        this.newproducerview.push(this.producermap.get(parseInt(this.newproducer)));
        this.newproducerview = this.removeDuplicateUsingSet(this.newproducerview);
        console.log('newproducerview' + this.newproducerview);
    }

    addMovie() {
        console.log('newMovie'+this.newMovie);

        this.movieservice.addMovie(this.newMovie).subscribe(resp => {
            console.log(resp);
            console.log(resp.mid);
            this.newMovie.lactor.forEach((value: string) => {
                var obj = new movievsactor();
                obj.maID = 0;
                obj.maactor = parseInt(value);
                obj.mamovie = resp.mid;
                console.log(obj);
                this.movieservice.addnewMovievsActor(obj).subscribe(resp => {
                    console.log(resp);
                });
            });
            this.newMovie.lproducer.forEach((value: string) => {
                var obj = new movievsproducer();
                obj.mpID = 0;
                obj.mpproducer = parseInt(value);
                obj.mpmovie = resp.mid;
                console.log(obj);
                this.movieservice.addnewMovievsProducer(obj).subscribe(resp => {
                    console.log(resp);
                });
            });
            this.refresh();
        });
    }
    editMovie(movie_data: movies) {
        this.movieToEdit = movie_data;
        this.editmoviestate = true;
        console.log(this.movieToEdit);
    }

    deleteMovie(movie_data: movies) {
        console.log(movie_data);
        this.movieservice.deleteMovie(movie_data.mid);
        this.refresh();
    }

    UpdateMovie(movie_data: movies) {
        console.log(movie_data);
        this.movieservice.updateMovie(movie_data);
        this.refresh();
        this.clearEditMovie();
    }

    clearEditMovie() {
        this.movieToEdit = new movies();
        this.editmoviestate = false;
        this.refresh();
    }

    refresh(): void {
        this.getMovieList()
    }

    removeDuplicateUsingSet(arr: any) : any {
        let unique_array = Array.from(new Set(arr))
        return unique_array
    }

    createnewhero() {
        console.log(this.createnewhero_name);
        let actorobj = new actors();
        actorobj.aname = this.createnewhero_name;
        actorobj.agender = 'M';
        actorobj.adob = new Date('1970-01-01');
        actorobj.abio = '';
        console.log(actorobj);
        this.actorservice.addActor(actorobj).subscribe(resp => {
            console.log(resp);
            this.getActors();
        });
    }

    createnewproducer() {
        console.log(this.createnewproducer_name);
        console.log(this.createnewhero_name);
        let prodobj = new producers();
        prodobj.pname = this.createnewproducer_name;
        prodobj.pgender = 'M';
        prodobj.pdob = new Date('1970-01-01');
        prodobj.pbio = '';
        console.log(prodobj);
        this.producerservice.addProducer(prodobj).subscribe(resp => {
            console.log(resp);
            this.getProducers();
        });        
    }
}
