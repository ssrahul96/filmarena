﻿import { Component } from '@angular/core';
import { producers } from '../../model/producers';
import { ProducerService } from '../../services/producer.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';

@Component({
    selector: 'app-producers',
    templateUrl: './producers.component.html',
    styleUrls: ['./producers.component.css']
})
export class ProducersComponent implements OnInit {
    producerCollection: producers[];
    editproducerstate: boolean;
    producerToEdit: producers;
    newProducer: any;

    ngOnInit(): void {
        this.getProducers();
        this.editproducerstate = false;
    }

    constructor(private producerService: ProducerService) {
        this.newProducer = {
            pid: 0,
            pname: '',
            pdob: new Date(),
            pgender: '',
            pbio: ''
        };
    }

    getProducers(): void {
        this.producerService.getProducers().subscribe(producerdatas => {
            console.log(producerdatas);
            this.producerCollection = producerdatas;
        },
            err => {
                //console.log(err);
            })
    }

    addProducer() {
        console.log(this.newProducer);
        this.producerService.addProducer(this.newProducer);
        this.refresh();
    }
    editProducer(producer_data: producers) {
        this.producerToEdit = producer_data;
        this.editproducerstate = true;
        console.log(this.producerToEdit);
    }

    deleteProducer(producer_data: producers) {
        console.log(producer_data);
        this.producerService.deleteProducer(producer_data.pid);
        this.refresh();
    }

    UpdateProducer(producer_data: producers) {
        console.log(producer_data);
        this.producerService.updateProducer(producer_data);
        this.refresh();
        this.clearEditProducer();
    }

    clearEditProducer() {
        this.producerToEdit = new producers();
        this.editproducerstate = false;
    }

    refresh(): void {
        this.getProducers();
    }
}