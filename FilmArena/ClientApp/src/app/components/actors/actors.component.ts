﻿import { Component } from '@angular/core';
import { ActorService } from '../../services/actor.service';
import { actors } from '../../model/actors';

@Component({
    selector: 'app-actors',
    templateUrl: './actors.component.html',
    styleUrls: ['./actors.component.css']
})

export class ActorsComponent {
    actorCollection: actors[];
    editactorstate: boolean;
    actorToEdit: actors;
    newActor: any;

    ngOnInit(): void {
        this.getActors();
        this.editactorstate = false;
    }

    constructor(private actorService: ActorService) {
        this.newActor = {
            aid: 0,
            aname: '',
            adob: new Date(),
            agender: '',
            abio: ''
        };
    }

    getActors(): void {
        this.actorService.getActors().subscribe(actordatas => {
            console.log(actordatas);
            this.actorCollection = actordatas;
        },
            err => {
                //console.log(err);
            })
    }

    addActor() {
        console.log(this.newActor);
        this.actorService.addActor(this.newActor);
        this.refresh();
    }
    editActor(actor_date: actors) {
        this.actorToEdit = actor_date;
        this.editactorstate = true;
        console.log(this.actorToEdit);
    }

    deleteActor(actor_date: actors) {
        console.log(actor_date);
        this.actorService.deleteActor(actor_date.aid);
        this.refresh();
    }

    UpdateActor(actor_date: actors) {
        console.log(actor_date);
        this.actorService.updateActor(actor_date);
        this.refresh();
        this.clearEditActor();
    }

    clearEditActor() {
        this.actorToEdit = new actors();
        this.editactorstate = false;
    }

    refresh(): void {
        this.getActors()
    }
}