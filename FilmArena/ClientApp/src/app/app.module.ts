import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { ActorsComponent } from './components/actors/actors.component';
import { ProducersComponent } from './components/producers/producers.component';
import { MoviesService } from './services/movies.service';
import { ProducerService } from './services/producer.service';
import { ActorService } from './services/actor.service';
import { AppComponent } from './components/app/app.component';
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    ActorsComponent,
    ProducersComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', redirectTo: 'home', pathMatch: 'full' },
      { path: 'home', component: HomeComponent },
      { path: 'actors', component: ActorsComponent },
      { path: 'producers', component: ProducersComponent },
      { path: '**', redirectTo: 'home' }
    ])
  ],
  providers: [MoviesService, ProducerService, ActorService],
  bootstrap: [AppComponent]
})
export class AppModule { }
