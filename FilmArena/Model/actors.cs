﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmArena.Controllers.Model
{
    public class actors
    {
        [Key]
        public int aid { get; set; }
        public String aname { get; set; }
        public DateTime adob { get; set; }
        public String agender { get; set; }
        public String abio { get; set; }
    }
}
