﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmArena.Model
{
    public class movievsactors
    {
        [Key]
        public int maID { get; set; }
        public int mamovie { get; set; }
        public int maactor { get; set; }
    }
}
