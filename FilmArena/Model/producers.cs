﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmArena.Controllers.Model
{
    public class producers
    {
        [Key]
        public int pid { get; set; }
        public String pname { get; set; }
        public DateTime pdob { get; set; }
        public String pgender { get; set; }
        public String pbio { get; set; }
    }
}
