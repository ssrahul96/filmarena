﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmArena.Controllers.Model
{
    public class movies
    {
        [Key]
        public int mid { get; set; }
        public String mname { get; set; }
        public int myear { get; set; }
        public String mplot { get; set; }
        public String mposter { get; set; }
    }

    public class movielist : movies
    {
        public List<actors> lactor { get; set; }
        public List<producers> lproducers { get; set; }
    }
}
