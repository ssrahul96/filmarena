﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FilmArena.Model
{
    public class movievsproducers
    {
        [Key]
        public int mpID { get; set; }
        public int mpmovie { get; set; }
        public int mpproducer { get; set; }
    }
}
