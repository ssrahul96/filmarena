CREATE DATABASE film_arena;

CREATE TABLE movies (
    mid int NOT NULL AUTO_INCREMENT,
    mname varchar(255) NOT NULL,
    myear int,
    mplot varchar(50),
    mposter varchar(255),
    PRIMARY KEY (mid)
);


CREATE TABLE actors (
    aid int NOT NULL AUTO_INCREMENT,
    aname varchar(100) NOT NULL,
    agender varchar(1),
    adob date,
    abio varchar(255),
    PRIMARY KEY (aid)
);


CREATE TABLE producers (
    pid int NOT NULL AUTO_INCREMENT,
    pname varchar(100) NOT NULL,
    pgender varchar(1),
    pdob date,
    pbio varchar(255),
    PRIMARY KEY (pid)
);

CREATE TABLE movievsactors (
    maID int NOT NULL AUTO_INCREMENT,
    mamovie int NOT NULL,
    maactor int NOT NULL,
    PRIMARY KEY (maID,mamovie,maactor)
);

CREATE TABLE movievsproducers (
    mpID int NOT NULL AUTO_INCREMENT,
    mpmovie int NOT NULL,
    mpproducer int NOT NULL,
    PRIMARY KEY (mpID,mpmovie,mpproducer)
);